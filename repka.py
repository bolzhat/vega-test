# -*- coding: utf-8 -*-


def vinit(word):
    """Приводит существительное в винительный падеж"""

    if isinstance(word, str):
        word = word.decode('utf-8')
        utf = True
    else:
        utf = False

    try:
        word = word[:-1] + {u'я': u'ю', u'а': u'у'}[word[-1]]
    except KeyError:
        pass

    if utf:
        return word.encode('utf-8')
    else:
        return word


def repka(pullers, first=True):
    """Выводит сказку про репку, pullers - список тянущих репку"""

    if len(pullers) == 1:
        line = pullers[0] + ' за репку —'
        print line
        print 'Тянет-потянет, вытянуть не может.'
    elif not pullers:
        return
    else:
        line = '{} за {}, {}'.format(
            pullers[-1],
            vinit(pullers[-2]),
            repka(pullers[:-1], first=False),
        )
        print line

        if first:
            print 'Тянут-потянут — и вытянули репку.'
        else:
            print 'Тянут-потянут, вытянуть не могут.'

    return line


def main():
    pullers = ['Дедка', 'Бабка', 'Внучка', 'Жучка', 'Кошка', 'Мышка']
    repka(pullers)
    #print reduce(lambda x, y: y + ' за ' + x, pullers, 'репка')


if __name__ == '__main__':
    main()
