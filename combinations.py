# -*- coding: utf-8 -*-

import itertools


def main():
    # Load numbers from file
    with open('combinations.txt') as f:
        line = f.readlines()[0]

    numbers = sorted(int(number) for number in line.split(','))

    print numbers

    # Binary operations
    for a, b, c in itertools.permutations(numbers, 3):
        if a + b == c:
            print '{} + {} = {}'.format(a, b, c)

        if a - b == c:
            print '{} - {} = {}'.format(a, b, c)

        if a * b == c:
            print '{} * {} = {}'.format(a, b, c)
            if a:
                print '{} / {} = {}'.format(c, a, b)

        if (a or b >= 0) and a ** b == c:
            print '{} ** {} = {}'.format(a, b, c)

    # Unary operations
    for a, b in itertools.permutations(numbers, 2):
        if 2 ** a == b:
            print 'log2 {} = {}'.format(b, a)


if __name__ == '__main__':
    main()
