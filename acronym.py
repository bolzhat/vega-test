# -*- coding: utf-8 -*-

import re
import random


def main():
    # Build a dictionary from raw text
    dictionary = {}

    with open('acronym.txt') as f:
        for line in f:
            words = re.split(ur'[\W\d_]+', line, re.U)
            for word in words:
                if word:
                    word = word.title()
                    letter = word[0]
                    if letter in dictionary:
                        dictionary[letter].add(word)
                    else:
                        dictionary[letter] = set([word])

    for letter in dictionary:
        dictionary[letter] = list(dictionary[letter])

    # Convert acronyms from input
    while 1:
        try:
            s = raw_input('>> ')
        except:
            break

        s = s.upper()
        output = ''

        for char in s:
            if char in dictionary:
                word = random.choice(dictionary[char])
            else:
                word = char

            output = '{}{} '.format(output, word)

        print output


if __name__ == '__main__':
    main()
