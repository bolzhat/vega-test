#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import hmac
import base64
import getpass
import argparse


def add_hmac(filename, password):
    """Add HMAC to the end of file"""

    with open(filename, 'rb') as f:
        code = hmac.new(
            key=password,
            msg=f.read(),
        ).digest()

    code = base64.b64encode(code)

    with open(filename, 'ab') as f:
        f.write('\n' + code + '\n')


def check_hmac(filename, password):
    """Check file integrity, return True, if it's not corrupted"""

    with open(filename, 'rb') as f:
        content = f.read()

    # Find HMAC in the end of file
    parts = re.split(r'\n([^\n]*)\n$', content, maxsplit=1)
    if len(parts) != 3:
        return False

    msg, code, _ = parts

    code = base64.b64decode(code)

    original_code = hmac.new(
        key=password,
        msg=msg,
    ).digest()

    return code == original_code


def main():
    parser = argparse.ArgumentParser(description='File authentication')
    parser.add_argument(
        '-c',
        action='store_true',
        help='check file integrity',
    )
    parser.add_argument(
        'file',
        type=str,
        help='file name',
    )
    args = parser.parse_args()
    password = getpass.getpass()

    if args.c:
        if check_hmac(args.file, password):
            print args.file, 'verified'
        else:
            print args.file, 'corrupted'
    else:
        add_hmac(args.file, password)
        print 'Added HMAC to the end of', args.file


if __name__ == '__main__':
    main()
